<?php

class Conexao{
    var $servidor = '192.168.50.20';
    var $usuario = 'sa';
    var $senha = 'k14yg93';
    
    function conecta($banco){
        try{
            $conn = new \PDO("dblib:host=$this->servidor;dbname=$banco", $this->usuario, $this->senha);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $conn;
        }
        catch(PDOException $e){
            $erro = array(
                'erro_banco' => $e->getMessage()
            );
            echo json_encode($erro);
            die;
        }
    }
}

$conexao = new Conexao();
$pdo = $conexao->conecta('titoibroker');

$sql  = "SELECT         ".PHP_EOL;
$sql .= "    COUNT(PROCESSO.IDVIATRANSPORTE) AS 'QNTD', ".PHP_EOL;
$sql .= "    VIA.NMVIATRANSPORTE AS 'LABEL',    ".PHP_EOL;
$sql .= "    MONTH(DICAPA.DTREGISTRODI) AS 'MES',   ".PHP_EOL;
$sql .= "    YEAR(DICAPA.DTREGISTRODI) AS 'ANO' ".PHP_EOL;
$sql .= "FROM PROCESSO          ".PHP_EOL;
$sql .= "    LEFT JOIN PESSOA CLIENTE ON (CLIENTE.IDPESSOA = PROCESSO.IDPESSOACLIENTE)          ".PHP_EOL;
$sql .= "    LEFT JOIN REFERENCIACLIENTE on (REFERENCIACLIENTE.IDPROCESSO = PROCESSO.IDPROCESSO)            ".PHP_EOL;
$sql .= "    LEFT JOIN VIATRANSPORTE VIA ON (VIA.IDVIATRANSPORTE = PROCESSO.IDVIATRANSPORTE)            ".PHP_EOL;
$sql .= "    LEFT JOIN EMBARCACAO ON ( EMBARCACAO.IDEMBARCACAO = PROCESSO.IDEMBARCACAO)         ".PHP_EOL;
$sql .= "    LEFT JOIN DICAPA ON (DICAPA.IDPROCESSO = PROCESSO.IDPROCESSO)          ".PHP_EOL;
$sql .= "    LEFT JOIN MODALIDADEPAGAMENTO on (MODALIDADEPAGAMENTO.IDMODALIDADEPAGAMENTO=DICAPA.IDMODALIDADEPAGAMENTO)          ".PHP_EOL;
// $sql .= "    LEFT JOIN FOLLOWUPPROCESSO FUP ON FUP.IDPROCESSO = PROCESSO.IDPROCESSO         ".PHP_EOL;
$sql .= "    LEFT OUTER JOIN PROCESSOEXPORTACAO ON (PROCESSOEXPORTACAO.IDPROCESSO = PROCESSO.IDPROCESSO)    ".PHP_EOL;
$sql .= "WHERE      ".PHP_EOL;
$sql .= "    NRPROCESSO NOT LIKE 'LI%'      ".PHP_EOL;
$sql .= "AND PROCESSO.DTCANCELAMENTO IS NULL    ".PHP_EOL;
$sql .= "AND DICAPA.TPDECLARACAO IN (1) ".PHP_EOL;
$sql .= "AND NMVIATRANSPORTE NOT IN ('.')   ".PHP_EOL;
$sql .= "AND YEAR(DICAPA.DTREGISTRODI) IN (2021)  ".PHP_EOL;
//$sql .= "%%FILTRO%%".PHP_EOL;
$sql .= "AND PROCESSO.IDPESSOACLIENTE = '948'   ".PHP_EOL;
$sql .= "GROUP BY VIA.NMVIATRANSPORTE, MONTH(DICAPA.DTREGISTRODI), YEAR(DICAPA.DTREGISTRODI)    ".PHP_EOL;
$sql .= "ORDER BY MES   ".PHP_EOL;

$stmt = $pdo->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($result);
die;